# Rc hacks

This code exposes a few custom functions that are routinely useful to me. They are devised as quick access to repetitive tasks.

## 1. Quick crop volume functions

### Setup

Paste the implementation files in the logical sort order in .slicerrc.py : 00-Lib (once only) then 11-QuickCropVolume. After starting Slicer, a custom toolbar with a button offering a drop-down menu will appear.

### Usage

- *Crop volume to current ROI* : Select an ROI and click this menu. The background volume in the Red slice node will be cropped to the selected ROI without interpolation.
- *Crop volume to current ROI x 2* : Select an ROI and click this menu. The background volume in the Red slice node will be cropped to the selected ROI with a spacing scale of 0.5.
- *Crop volume to current ROI (Iso 0.3)* : Select an ROI and click this menu. The background volume in the Red slice node will be cropped to the selected ROI and resampled to an isovoxel spacing of 0.3 mm. The ROI should be reasonably sized according to available RAM and CPU.
- *Crop volume to current ROI (Iso 0.5)* : Select an ROI and click this menu. The background volume in the Red slice node will be cropped to the selected ROI and resampled to an isovoxel spacing of 0.5 mm. The ROI should be reasonably sized according to available RAM and CPU.

## 2. Reslice functions

### Setup

Paste the implementation files in the logical sort order in .slicerrc.py : 00-Lib (once only) then 12-ResliceToAxis. After starting Slicer, a custom toolbar with a button offering a drop-down menu will appear.

### Usage

Select a slice view, an axis and apply. The slice node will be reformatted to the currently selected markups line, plane, angle or fiducial node.

The axes are determined by the direction vector of the line, by the normal of the plane, or by the normal of the angle node at the middle point.

If a fiucial node is provided with a single point, it is itself used as a normal. If there are only 2 points, they are used as a line. If the fiducial node has 3 points, they are used as a plane. With more than 3 points, a best fit plane is determined according to the logic of a markups plane node.

## 3. MIS to tube functions

Create a tube from the 'Radius' scalar array of a non-bifurcated centerline model or curve.

Note: A bifurcated centerline model should be pre-processed with 'Centerline disassembly' module from SlicerVMTK to suppress bifurcations.

### Setup

Paste the implementation files in the logical sort order in .slicerrc.py : 00-Lib (once only) then 13-MisToTube. After starting Slicer, a custom toolbar with a button offering a drop-down menu will appear.


## 4. Markups Undo/Redo functions

### Setup

Paste the implementation files in the logical sort order in .slicerrc.py : 00-Lib (once only) then 14-UndoRedo. After starting Slicer, a custom toolbar with Undo/Redo buttons will appear.

### Usage

To undo an action on a markups node, click on the Undo (yellow) button or press 'Ctrl+Z'.

To redo an action on a markups node, click on the Redo (green) button or press 'Ctrl+Shift+Z'.

#### Notes

This [functionality](https://projectweek.na-mic.org/PW39_2023_Montreal/Projects/UndoRedo/) is added here with permission from the author, Kyle Sunderland. It has been further modified.

---

### License

This work, as a whole, is licensed under the terms of the [CeCILL-B](http://cecill.info/licences/Licence_CeCILL-B_V1-en.txt) free software license agreement (BSD style).

### Author

Saleem Edah-Tally [Surgeon] [Hobbyist developer]

### Disclaimer

Use at your own risks.
